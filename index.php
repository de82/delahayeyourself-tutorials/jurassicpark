<?php require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function($string){
        return renderHTMLFromMarkdown($string);
    }));
});


Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});


Flight::route('/', function(){
    $data = [
        'dinosaurs' => getDinosaurs(),
    ];
    Flight::render('index.twig', $data);
});

Flight::route('/dinosaur/@slug', function($slug){
    $data = [
        'dino' => getDinosaur($slug),
        'top_rated' => topRatedDinosaurs(3),
    ];
    Flight::render('dinosaur.twig', $data);
});


Flight::start();
