<?php require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTest extends TestCase {

    public function test_getDinosaurs()
    {
        $dinosaurs = getDinosaurs();
        $this->assertIsArray($dinosaurs);
        $this->assertEquals(7, count($dinosaurs));
        $this->assertIsString($dinosaurs[0]->name);
        $this->assertIsString($dinosaurs[0]->slug);
    }

    public function test_getDinosaur()
    {
        $dinosaur = getDinosaur('brachiosaurus');
        $this->assertInstanceOf(stdClass::class, $dinosaur);
        $this->assertIsString($dinosaur->slug);
    }

    public function test_topRatedDinosaurs()
    {
        $dinosaurs = topRatedDinosaurs(4);

        $this->assertIsArray($dinosaurs);
        $this->assertEquals(4, count($dinosaurs));
        $this->assertIsString($dinosaurs[0]->name);

    }

    public function test_renderHTMLFromMarkdown()
    {
        $this->assertEquals("<h2>Test</h2>", renderHTMLFromMarkdown("## Test"));
    }

}