<?php require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{

    public function test_index()
    {
        $dinosaurs = getDinosaurs();
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);

        $body = $response->getBody()->getContents();

        foreach($dinosaurs as $dinosaur)
        {
            $this->assertStringContainsString($dinosaur->name, $body);
            $this->assertStringContainsString($dinosaur->avatar, $body);
        }
        
    }

    public function test_dinosaur()
    {
        $dinosaur = getDinosaur('brachiosaurus');
        $response = $this->make_request("GET", "/dinosaur/brachiosaurus");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);

        $body = $response->getBody()->getContents();

        $this->assertStringContainsString($dinosaur->name, $body);
        $this->assertStringContainsString($dinosaur->avatar, $body);
        $this->assertStringContainsString($dinosaur->weight, $body);
        $this->assertStringContainsString($dinosaur->diet, $body);
        $this->assertStringContainsString($dinosaur->length, $body);
    }
}