<?php

use Michelf\Markdown;


function getDinosaurs()
{
    $response = Requests::get('https://medusa.delahayeyourself.info/api/dinosaurs/');
    return json_decode($response->body);
}

function getDinosaur($slug)
{
    $url = sprintf('https://medusa.delahayeyourself.info/api/dinosaurs/%s', $slug);
    $response = Requests::get($url);
    return json_decode($response->body);
}

function topRatedDinosaurs($total)
{
    $dinosaurs = getDinosaurs();
    $top_rated_dinosaurs = array();

    $keys = array_rand($dinosaurs, $total);
    foreach($keys as $key)
    {
        $top_rated_dinosaurs[] = $dinosaurs[$key];
    }

    return $top_rated_dinosaurs;
}

function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return trim(Markdown::defaultTransform($string_markdown_formatted));
}
